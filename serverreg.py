#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import json
import  threading
import time


class EchoHandler(socketserver.DatagramRequestHandler):
    """
    UDP echo handler class
    """

    registered = {}
    estado = {"puerta":["abierta"]}

    def json2registered(self):
        try:
            with open('register.json', "r") as jsonfile:
                self.registered == json.load(jsonfile)
        except OSError as err:
            pass

    def registered2json(self):
        with open('register.json', "w") as jsonfile:
            json.dump(self.registered, jsonfile, indent=4)

    def expiraciones(self):
        self.i = 0

        while True:
            self.i += 1
            time.sleep(1)
            try:
                if self.i == int(self.registered["singasong.net"][2]):
                    del self.registered["singasong.net"]
                    self.registered2json()
                if self.i == int(self.registered["clientes.net"][2]):
                    del self.registered["clientes.net"]
                    self.registered2json()
            except:
                pass

    def handle(self):
        if self.estado["puerta"] == "abierta":
            self.estado["puerta"] = "cerrada"
            self.json2registered()
            msg = self.rfile.read()
            msgCliente = msg.decode('utf-8')
            peticionCliente = msgCliente.split(" ")[0]
            direccion = msgCliente.split(" ")[1].split("@")[1]
            client = self.client_address
            expires = msgCliente.split("\r\n")[1].split(" ")[1]
            print(tiempo + " SIP from " + self.client_address[0] + ":" + str(self.client_address[1])+ ": "+ msg.decode().split("\r\n")[0] +".")
            self.registered[direccion] = self.client_address[0],self.client_address[1], expires
            if peticionCliente == "REGISTER":
                self.wfile.write(f"SIP/2.0 200 OK\r\n\r\n".encode())
                self.registered2json()
            else:
                self.wfile.write(f"SIP/2.0 405 Method Not Allowed\r\n\r\n".encode())
                self.registered[direccion] = self.client_address
                self.registered2json()
        else:
            t = threading.Thread(target=self.expiraciones)
            t.start()
            self.estado["puerta"] = "cerrada"
            msg = self.rfile.read()
            msgCliente = msg.decode('utf-8')
            peticionCliente = msgCliente.split(" ")[0]
            direccion = msgCliente.split(" ")[1].split("@")[1]
            client = self.client_address
            expires = msgCliente.split("\r\n")[1].split(" ")[1]
            self.registered[direccion] = self.client_address[0],self.client_address[1], expires
            print(tiempo + " SIP from " + self.client_address[0] + ":" + str(self.client_address[1])+ ": "+ msg.decode().split("\r\n")[0] +".")
            if peticionCliente == "REGISTER":
                self.wfile.write(f"SIP/2.0 200 OK\r\n\r\n".encode())
                self.registered[direccion] = self.client_address[0],self.client_address[1], expires
                self.registered2json()
            else:
                self.wfile.write(f"SIP/2.0 405 Method Not Allowed\r\n\r\n".encode())
                self.registered[direccion] = self.client_address
                self.registered2json()


def main():
    try:
        puertoReg = int(sys.argv[1])
        global tiempo
        tiempo = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time() + 3600))
        with socketserver.UDPServer(('',puertoReg ), EchoHandler) as serv:
            print(tiempo + " Starting...")
            serv.serve_forever()
    except:
        print("Usage: python3 serverrtp.py <port> <file>")

if __name__ == "__main__":
    main()
