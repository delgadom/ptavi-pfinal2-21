#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""
import socket
import socketserver
import sys
import json
import time


class EchoHandler(socketserver.DatagramRequestHandler):
    """
    UDP echo handler class
    """
    registered = {}

    def json2registered(self):
        try:
            with open('register.json', "r") as jsonfile:
                self.registered = json.load(jsonfile)
        except OSError as err:
            pass

    def send(self):
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
                print(tiempo + " SIP to " + self.dest_addr[0] + ":" + str(self.dest_addr[1]) + ": " + self.packet.decode().split("\r\n")[0] + ".")
                my_socket.sendto(self.packet, (self.dest_addr[0],int(self.dest_addr[1])))
                data = my_socket.recv(2048)
                if data.decode('utf-8') == "SIP/2.0 200 OK\r\n\r\n":
                    print(tiempo + " SIP from " + self.dest_addr[0] + ":" + str(self.dest_addr[1]) + ": " + data.decode().split("\r\n")[0] + ".")
                    self.json2registered()
                    self.dest_addr = self.registered["clientes.net"]
                    my_socket.sendto(data, (self.dest_addr[0],int(self.dest_addr[1])))
                    print(tiempo + " SIP to " + self.dest_addr[0] + ":" + str(self.dest_addr[1]) + ": " + data.decode().split("\r\n")[0] + ".")
                elif data.decode('utf-8') == "SIP/2.0 400 Bad Request\r\n\r\n":
                    print(tiempo + " SIP from " + self.dest_addr[0] + ":" + str(self.dest_addr[1]) + ": " + data.decode().split("\r\n")[0] + ".")
                    self.json2registered()
                    self.dest_addr = self.registered["clientes.net"]
                    my_socket.sendto(data, (self.dest_addr[0],int(self.dest_addr[1])))
                    print(tiempo + " SIP to " + self.dest_addr[0] + ":" + str(self.dest_addr[1]) + ": "+ data.decode().split("\r\n")[0] + ".")
                elif data.decode('utf-8') == "SIP/2.0 405 Method Not Allowed\r\n\r\n":
                    print(tiempo + " SIP from " + self.dest_addr[0] + ":" + str(self.dest_addr[1]) + ": "+ data.decode().split("\r\n")[0] + ".")
                    self.json2registered()
                    self.dest_addr = self.registered["clientes.net"]
                    my_socket.sendto(data, (self.dest_addr[0],int(self.dest_addr[1])))
                    print(tiempo + " SIP to " + self.dest_addr[0] + ":" + str(self.dest_addr[1])+ ": "+ data.decode().split("\r\n")[0] + ".")

    def handle(self):
        self.json2registered()
        msg = self.rfile.read()
        msgCliente = msg.decode('utf-8')
        client = self.client_address
        peticionCliente = msgCliente.split(" ")[0]
        print(tiempo + " SIP from " + self.client_address[0] + ":" + str(self.client_address[1]) + ": " + msg.decode().split("\r\n")[0] + ".")
        if peticionCliente == "SIP/2.0":
            self.json2registered()
            print("tienes que enviar el ok")
            self.dest_addr = self.registered["clientes.net"]
            self.send()
        else:
            self.json2registered()
            self.dest_addr = self.registered["singasong.net"]
            self.send()


def main():
    try:
        if len(sys.argv)== 3:
            puertoProxy = int(sys.argv[1])
            global tiempo
            tiempo = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time() + 3600))
            with socketserver.UDPServer(('', puertoProxy), EchoHandler) as serv:
                print(tiempo + " Starting...")
                serv.serve_forever()
        else:
            print("Usage: python3 serverproxy.py <port> <file>")
    except:
        print("Usage: python3 serverproxy.py <port> <file>")


if __name__ == "__main__":
    main()
