## Parte básica.
La práctica está compuesta por: Servidor registro, Proxy, Servidor RTP, Cliente.

El funcionamiento básico consiste en un servicio de emisión y recepción de audio utilizando VoIP (SIP y RTP).


## Parte adicional.

 * Cabecera de tamaño.
 Se ha añadido la cabecera "Content-Lenght" en las peticioens invite. Indicando el tamaño del pauqete en bytes.

 * Gestión de Errores.
 El programa gestiona los errores enviando las correspodientes respuestas de error en lugar 200 OK "SIP/2.0 400 Bad Request" y "SIP/2.0 405 Method Not Allowed"

 * Tiempo de expiracion.
 Se ha implementado en el método register el parámetro "Expires" que indica el tiempo en segundos que el servidor registrar guarda en los datos de quien se registre. Una vez finalizado este tiempo el servidor registrar eliminará de forma automatica todos los datos relacionados con el usuario expirado o servidor expirado.
 Los tiempos por defecto para cliente son 40 segundos y para servidor RT 9999 segundos.
