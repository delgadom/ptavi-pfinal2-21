#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import socket
import threading
import time
import simplertp


class RTPHandler(socketserver.BaseRequestHandler):
    """Clase para manejar los paquetes RTP que nos lleguen.

    Atención: Heredamos de BaseRequestHandler porque UDPRequestHandler
    siempre termina enviando una respuesta al cliente, algo que aquí no
    queremos hacer. Al usar BaseRequestHandler no podemos usar self.rfile
    (porque esta clase no lo tiene), así que leemos el mensaje directamente
    de self.request (que es una lista, cuyo primer elemento es el mensaje
    recibido)"""

    def handle(self):
        msg = self.request[0]
        # sólo nos interesa lo que hay a partir del byte 54 del paquete UDP
        # que es donde está la payload del paquete RTP qeu va dentro de él
        payload = msg[12:]
        # Escribimos esta payload en el fichero de salida
        self.output.write(payload)
        print("Received")

    @classmethod
    def open_output(cls, filename):
        """Abrir el fichero donde se va a escribir lo recibido.

        Lo abrimos en modo escritura (w) y binario (b)
        Es un método de la clase para que podamos invocarlo sobre la clase
        antes de empezar a recibir paquetes."""

        cls.output = open(filename, 'wb')

    @classmethod
    def close_output(cls):
        """Cerrar el fichero donde se va a escribir lo recibido.

        Es un método de la clase para que podamos invocarlo sobre la clase
        después de terminar de recibir paquetes."""
        cls.output.close()


class SIPHandler(socketserver.DatagramRequestHandler):
    """
    UDP echo handler class
    """
    estado = {"puerta": ["abierta"]}

    def send(self):
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
                data = f"{self.metodo} {self.dirServidorRTP} SIP/2.0\r\n"
                my_socket.sendto(data.encode('utf-8'), (self.IPProxy, int(self.puertoProxy)))

    def recibirRTP(self):
        RTPHandler.open_output(self.archivo)
        with socketserver.UDPServer(("", 6010), RTPHandler) as serv:
            print(self.tiempo + " RTP ready 6010.")
            # El bucle de recepción (serv_forever) va en un hilo aparte,
            # para que se continue ejecutando este programa principal,
            # y podamos interrumpir ese bucle más adelante
            threading.Thread(target=serv.serve_forever).start()
            # Paramos un rato. Igualmente podríamos esperar a recibir BYE,
            # por ejemplo
            time.sleep(self.OutTime)
            # Paramos el bucle de recepción, con lo que terminará el thread,
            # y dejamos de recibir paquetes
            serv.shutdown()
        # Cerramos el fichero donde estamos escribiendo los datos recibidos
        RTPHandler.close_output()

    def handle(self):
        self.tiempo = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time() + 3600))
        SERVER = 'localhost'
        PORT = 6001
        self.OutTime = int(sys.argv[5])
        IPReg = sys.argv[1].split(":")[0]
        puertoReg = int(sys.argv[1].split(":")[1])
        self.IPProxy = sys.argv[2].split(":")[0]
        self.puertoProxy = int(sys.argv[2].split(":")[1])
        self.dirServidorRTP = sys.argv[4]
        UsuarioSIP = sys.argv[4].split("@")[0].split(":")[1]
        msg = self.rfile.read()
        msgCliente = msg.decode('utf-8')
        client = self.client_address
        peticionProxy = msgCliente.split(" ")[1]
        self.archivo = sys.argv[6]
        print(self.tiempo + " SIP from " + client[0] + ":" + str(client[1]) + ":" + " " + msgCliente.split("\r\n")[0] + ".")
        if peticionProxy == "200" and self.estado["puerta"] != "cerrada":
            self.estado["puerta"] = "cerrada"
            self.metodo = "ACK"
            self.send()
            print(self.tiempo + " SIP to " + self.IPProxy + ":" + str(self.puertoProxy) + ":" + f"{self.metodo} {self.dirServidorRTP} SIP/2.0" + ".")
            self.metodo = "BYE"
            self.recibirRTP()
            print(self.tiempo + " SIP to " + self.IPProxy + ":" + str(self.puertoProxy) + ":" + f"{self.metodo} {self.dirServidorRTP} SIP/2.0" + ".")
            self.send()
        else:
            exit()


def main():
    # Creamos servidor de eco y escuchamos
    if len(sys.argv) == 7:
            tiempo = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time() + 3600))
            SERVER = 'localhost'
            PORT = 6001
            IPReg = sys.argv[1].split(":")[0]
            puertoReg = int(sys.argv[1].split(":")[1])
            IPProxy = sys.argv[2].split(":")[0]
            puertoProxy = int(sys.argv[2].split(":")[1])
            dirServidorRTP = sys.argv[4]
            UsuarioSIP = sys.argv[4].split("@")[0].split(":")[1]
            PuertoPaquetes = 6010
            print(tiempo + " Starting...")
            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
                    my_socket.bind(('', 6006))
                    my_socket.connect((IPReg, puertoReg))
                    data = f"REGISTER sip:yo@clientes.net SIP/2.0\r\nExpires: 3600\r\n\r\n"
                    print(tiempo + " SIP to " + IPReg + ":" + str(puertoReg) + ": " + "REGISTER sip:yo@clientes.net SIP/2.0" + ".")
                    my_socket.sendto(data.encode('utf-8'), (IPReg, puertoReg))
                    data = my_socket.recv(2048)
                    print(tiempo + " SIP from " + IPReg + ":" + str(puertoReg) + ": " + data.decode('utf-8').split("\r\n")[0] + ".")
            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
                    cuerpo = f"v=0\r\n" + f"o={dirServidorRTP} 127.0.0.1\r\n" + f"s={UsuarioSIP}\r\n" + f"t=0\r\n" + f"m=audio {PuertoPaquetes} RTP\r\n"
                    BodySize = "Content-Length: " + str(sys.getsizeof(cuerpo)) + "\r\n\r\n"
                    datos = f"INVITE {dirServidorRTP} SIP/2.0\r\n" + 'Content-Type: application/sdp\r\n' + BodySize + cuerpo
                    print(tiempo + " SIP to " + IPProxy + ":" + str(puertoProxy) + ": " + datos.split("\r\n")[0] + ".")
                    my_socket.sendto(datos.encode('utf-8'), (IPProxy, puertoProxy))
            with socketserver.UDPServer(("127.0.0.1", 6006), SIPHandler) as serv:
                serv.serve_forever()
    else:
        print("Usage: python3 client.py <IPReg>:<portReg> <IPProxy>:<portProxy> <addrClient> <addrServerRTP> <time> <file>")

if __name__ == "__main__":
    main()
