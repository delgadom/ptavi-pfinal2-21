#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import socket
import simplertp
import time
import  threading

class RTPHandler(socketserver.DatagramRequestHandler):
    """
    UDP echo handler class
    """
    dicc = {}

    def handle(self):
        global sender
        try:
            self.NombreFichero = sys.argv[2].split(".")[0]
            msg = self.rfile.read()
            self.msgCliente = msg.decode('utf-8')
            client = self.client_address
            peticionProxy = self.msgCliente.split(" ")[0]
            print(tiempo + " SIP from " + self.client_address[0] + ":" + str(self.client_address[1])+ ": "+ msg.decode().split("\r\n")[0] +".")
            if peticionProxy in ['INVITE', 'ACK', 'BYE']:
                try:
                    if peticionProxy == "INVITE":
                        self.dicc["PuertoEnvio"] = int(self.msgCliente.split("\r\n")[8].split(" ")[1])
                        self.dicc["IpEnvio"] = self.msgCliente.split("\r\n")[5].split(" ")[1]
                        envio = f"SIP/2.0 200 OK\r\n\r\n"
                        self.wfile.write(envio.encode())
                        print(tiempo + " SIP to " + self.client_address[0] + ":" + str(self.client_address[1])+ ": "+ envio.split("\r\n")[0] +".")
                    elif peticionProxy == "ACK":
                        sender = simplertp.RTPSender(ip=self.dicc["IpEnvio"], port=self.dicc["PuertoEnvio"], file="cancion.mp3", printout=True)
                        sender.send_threaded()
                    elif peticionProxy == "BYE":
                        sender.finish()
                        envio = f"SIP/2.0 200 OK\r\n\r\n"
                        self.wfile.write(envio.encode())
                        print(tiempo + " SIP to " + self.client_address[0] + ":" + str(self.client_address[1])+ ": "+ envio.split("\r\n")[0] +".")
                    else:
                        pass
                except:
                    envio = f"SIP/2.0 400 Bad Request\r\n\r\n"
                    self.wfile.write(envio.encode())
            else:
                envio = f"SIP/2.0 405 Method Not Allowed\r\n\r\n"
                self.wfile.write(envio.encode())
        except:
            envio = f"SIP/2.0 400 Bad Request\r\n\r\n"
            self.wfile.write(envio.encode())


def main():
    try:
        if len(sys.argv)== 3:
            global tiempo
            tiempo = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time() + 3600))
            print(tiempo + " Starting...")
            IPReg = sys.argv[1].split(":")[0]
            puertoReg = int(sys.argv[1].split(":")[1])
            NombreFichero = sys.argv[2].split(".")[0]
            with socketserver.UDPServer(('0.0.0.0', 0), RTPHandler) as serv:
                print(tiempo + " SIP to " + IPReg + ":" + str(puertoReg)+ ": " + "REGISTER sip:"+ NombreFichero +"@singasong.net SIP/2.0" + ".")
                data = f"REGISTER sip:{NombreFichero}@singasong.net SIP/2.0\r\nExpires: 9999\r\n\r\n"
                serv.socket.sendto(data.encode('utf-8'), (IPReg, puertoReg))
                data = serv.socket.recv(2048)
                print(tiempo + " SIP from " + IPReg + ":" + str(puertoReg)+ ": " + data.decode('utf-8').split("\r\n")[0]+".")
                serv.serve_forever()
        else:
            print("Usage: python3 serverrtp.py <IPReg>:<portReg> <file>")
    except:
        print("Usage: python3 serverrtp.py <IPReg>:<portReg> <file>")


if __name__ == "__main__":
    main()
